/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patthamawan.counter1;

/**
 *
 * @author anurak
 */
public class Main {
    public static void main(String[] args) {
        Counter1 counter11 = new Counter1();
        Counter1 counter2 = new Counter1();
        Counter1 counter3 = new Counter1();
        counter11.increase();
        counter11.increase();
        counter11.increase();
        counter11.print();
        counter2.print();
        counter2.increase();
        counter2.print();
        counter2.decrease();
        counter2.print();
        counter11.increase();
        counter3.decrease();
        System.out.println("Print All Counter");
        counter11.print();
        counter2.print();
        counter3.print();
    }
    
}
